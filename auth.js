const axios = require ('axios')

exports.getSubject = decoded => decoded.payload.email

exports.isAuthorized = (decoded, request, callback, unauthorized, internalServerError, config) => {
  
  const email = decoded.sub.toLowerCase ()
  const domain = email.replace (/.*@/, "")

  const allowed_addresses = Array.isArray (config.ALLOWED_EMAIL_ADDRESSES) ?
                              config.ALLOWED_EMAIL_ADDRESSES.map (a => a.trim ().toLowerCase ()) :
                              []

  const allowed_domains = Array.isArray (config.ALLOWED_EMAIL_DOMAINS) ?
                              config.ALLOWED_EMAIL_DOMAINS.map (a => a.trim ().toLowerCase ()) :
                              []

  const in_addresses = allowed_addresses.some (a => a == email)
  const in_domains = allowed_domains.some (d => d == domain)

  if (in_addresses || in_domains) {
    callback (null, request)
     return
  }

  unauthorized ('Unauthorized', 'User '+email+' is not permitted.', '', callback)
}
