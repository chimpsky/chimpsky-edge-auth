Todo
====

 * refactor & document the whole shebang
 * work out how to allow @toruinteractive.com emails when on another site (i.e. dev.yoshie.cloud)
 * automatic deloyment + feedback
 * look into making this work as a npm package in another project
 * Move sensitive data into environmental variables
 * instead of instantly redirecting to Google Auth when you hit a private URI, send to login screen instead with button
 * look into working [locally on Lambda functions][1]


[1]: https://hackernoon.com/running-and-debugging-aws-lambda-functions-locally-with-the-serverless-framework-and-vs-code-a254e2011010