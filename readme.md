# Chimpsky Edge Auth

This repo puts content published to S3 by  [Chimpsky][1] behind a Google Signin.

It works by generating a Lambda@Edge script that screens URLs and redirects traffic to Google Signin if the user's not signed in. 

## Initial Setup

Create Google API credentials...

* goto https://console.cloud.google.com/apis/dashboard
* create a new project
* Select credentials
* select OAuth -> web application (you might need to fill some stuff out first)
* Set your domain as the authorised origin (e.g. https://www.example.com)
* Append `/_callback` for the Authorised Redirect URL (e.g. https://www.example.com/_callback)
* Save your Client ID and Client Secret somewhere safe

## Development

prepare Code for Deployment...

* duplicate `./config.template.json` to `./config.json` (which is ignored as you'll be storing sensitive data in there)
* Set the following fields in `./config.json`
  - replace the __GOOGLE_CLIENT_ID__ and __GOOGLE_CLIENT_SECRET__ fields with your id and secret (from step 1)
  - change any fields that includes example.com to work with your domain
  - PUBLIC PRIVATE KEY STUFF
  - Adjust the bypass pattern if you want to change the default behaviour of allowing only the main index to load without auth.
* Adjust ALLOWED_EMAIL_ADDRESSES and ALLOWED_EMAIL_DOMAINS in config to set who you want to let through
* log into the TI AWS and goto [this page](https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions/Readme_Cloudfront_Auth?tab=configuration)
 * copy the PRIVATE_KEY and PUBLIC_KEY values onto your config's PRIVATE_KEY and PUBLIC KEY
  - NOTE: WE NEED TO IMPROVE THIS PROCESS SO WE'RE MAKING OUR OWN KEYS

## Deployment

 * Sign into the `TI Base > Toru Interactive` AWS account
 * goto the Lambda function on the North Viginia region
 * under `Function Code` > `Code Entry Type` choose `Upload a .zip file`
 * Upload your .zip. Save the function. 
 * `actions` > `publish`, give it a version number. 
 * Copy the new ARN (top right)
 * goto the  Distributions
 * select your distribution, edit it's only behaviour
 * Under `Lambda Function Associations` paste the new function ARN over the old one & save

## quicker development via AWS console

COPY ALL MEANINGFUL CHANGES BACK TO THIS REPO OR YOUR HARD WORK WILL BE OVERWRITTEN AT SOME POINT IN THE FUTURE!!!

 * goto the latest version of your function
 * make your changes to the code inline via the `Function Code` panel
 * configure and run test events (top right)

---

Note: This was Based on Widen's [Cloudfront Auth][2] library. It references an OAuth token generated on [Google Console][3].

[1]: https://gitlab.com/chimpsky/chimpsky
[2]: https://github.com/widen/cloudfront-auth
[3]: https://console.developers.google.com/
[4]: https://regex101.com